import React from 'react'
import { ResponsiveBar } from '@nivo/bar'

const data = [
  { month: "january", payments: 333823 },
  { month: "february", payments: 289877 },
  { month: "march", payments: 207712 },
  { month: "april", payments: 21550 },
  { month: "may", payments: 60084 }
];

export default function App3Charts() {

  return (
    <div style={{height: '400px'}}>
      <p>This chart and its vendor dependency have been code split from the App3 bundle</p>
      <ResponsiveBar 
        data={data}
        keys={['payments']}
        indexBy="month"
        colors={{ scheme: "nivo" }}
      />
    </div>
  )
}
