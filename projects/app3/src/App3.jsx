import React from 'react'
import { Routes, Route, Link } from 'react-router-dom'

const App3ChartsLazy = React.lazy(() => import('./App3Charts'))

const App3Root = () => {
  return (
    <div>
      <p>This is the basic root page, click on the other link to see code splitting from this bundle that includes spicy vendor dependencies</p>
    </div>
  )
}

export default function App3() {
  return <div>
    <h1 className="text-4xl mb-4">App3</h1>
    <p className="mb-2">This project is a more advanced version of a project</p>
    <p className="mb-12">This project includes its own dependencies and code-splitting</p>

    <div className="border-b mb-8">
      <Link to="" className="mr-4">App3 Root</Link>
      <Link to="charts">App3 Charts</Link>
    </div>

    <Routes>
      <Route path="" element={<App3Root />} />
      <Route path="charts" element={<App3ChartsLazy />} />
    </Routes>

  </div>
}
