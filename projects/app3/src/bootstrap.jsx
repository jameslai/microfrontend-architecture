import React from 'react'
import { createRoot } from 'react-dom'
import App3 from './App3.jsx'

const container = document.getElementById('root')
const root = createRoot(container)
root.render(<App3 />)


