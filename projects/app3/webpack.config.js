const path = require('path')
const deps = require('./package.json').dependencies
const ModuleFederationPlugin = require('webpack').container.ModuleFederationPlugin

// Customize for your application name
const projectName = 'app3'

// Must be unique per project
const port = 8083

// Less customized options

const entry = './src/index'

// Configuration settings for the development server
const devServer = {
  port,
  historyApiFallback: true,
  headers: {
    'Access-Control-Allow-Origin': '*'
  },
  static: {
    directory: path.join(__dirname, 'static')
  }
}

const resolve = {
  extensions: ['.js', '.jsx']
}

// Module configuration
const moduleDef = {
  rules: [
    {
      test: /.jsx$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env', '@babel/preset-react']
        }
      }
    }
  ]
}

const plugins = [
  new ModuleFederationPlugin({
    name: projectName,
    filename: 'remoteEntry.js',
    exposes: {
      './App3': './src/App3'
    },
    remotes: {
      // 'projects/shell': 'shell@http://localhost:8080/remoteEntry.js'
    },
    shared: {
      ...deps,
      react: {
        singleton: true,
        requiredVersion: deps.react,
      },
      "react-dom": {
        singleton: true,
        requiredVersion: deps['react-dom']
      }
    }
  })
]

module.exports = {
  devServer,
  entry,
  resolve,
  module: moduleDef,
  plugins,
}
