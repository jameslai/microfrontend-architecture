const path = require('path')
const deps = require('./package.json').dependencies
const ModuleFederationPlugin = require('webpack').container.ModuleFederationPlugin

// Customize for your application name
const projectName = 'shell'

// Must be unique per project
const port = 8080

// Configuration settings for the development server
const devServer = {
  port,
  historyApiFallback: true,
  headers: {
    'Access-Control-Allow-Origin': '*'
  },
  static: {
    directory: path.join(__dirname, '../../static')
  }
}

const entry = './src/index'

const resolve = {
  extensions: ['.js', '.jsx']
}

// Module configuration
const moduleDef = {
  rules: [
    {
      test: /.jsx$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env', '@babel/preset-react']
        }
      }
    }
  ]
}

const plugins = [
  new ModuleFederationPlugin({
    name: 'shell',
    filename: 'remoteEntry.js',
    remotes: {
      app1: 'app1@http://localhost:8081/remoteEntry.js',
      app2: 'app2@http://localhost:8082/remoteEntry.js',
      app3: 'app3@http://localhost:8083/remoteEntry.js',
    },
    shared: {
      ...deps,
      react: {
        singleton: true,
        requiredVersion: deps.react,
      },
      "react-dom": {
        singleton: true,
        requiredVersion: deps['react-dom']
      },
    }
  })
]

module.exports = {
  devServer,
  entry,
  resolve,
  module: moduleDef,
  plugins,
}
