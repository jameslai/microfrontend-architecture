// Third Party
import React, { lazy, Suspense } from 'react'
import { Routes, Route, NavLink } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'

// Dynamic
const App1Lazy = lazy(() => import('app1/App1'))
const App2Lazy = lazy(() => import('app2/App2'))
const App3Lazy = lazy(() => import('app3/App3'))

const Home = () => {
  return (
    <div>
      <h1 className="text-4xl mb-4">Home</h1>
      <p className="mb-2">This is an example of a microfrontend architecture. Each navigation item houses a completely separate project that is independent of all other projects.</p>

      <p className="text-lg mb-1 font-semibold">How is this different from code-splitting?</p>
      <p className="mb-2">Code splitting allows developers to split a <span className="font-bold">single project</span> into multiple artifacts. The artifacts produced from code splitting are children of the project and deeply dependent upon it. They cannot be built separately from the project.</p>
      <p className="mb-4">In this microfrontend architecture, each navigation item is its own project. They define their own bundler, dependencies, state, scripts, etc. Each project can be independently built and shipped without processing any other aspect of the application.</p>

      <p className="text-lg mb-1 font-semibold">Advantages</p>
      <ul className="mb-2 ml-4">
        <li>Each project is its own ecosystem, allowing for easier integration when adopting acquired code</li>
        <li>Each project can be shipped individually (with some exceptions), allowing for substantially more efficient builds and tests, and introducing the possibly of shipping changes at the project level</li>
        <li>Clear separation of ownership of state</li>
      </ul>
      <p className="mb-4">See the <a href="https://module-federation.io/docs/en/mf-docs/0.2/pros-cons/" className="text-blue-500">Module Federation's documentation on Pros and Cons</a> for a complete list</p>

      <p className="text-lg mb-1 font-semibold">Todo</p>
      <ul className="mb-2 ml-4">
        <li><a href="https://webpack.js.org/concepts/module-federation/#dynamic-remote-containers" className="text-blue-500">Dynamically load remote containers</a></li>
        <li>Resolve duplicate vendor artifacts</li>
      </ul>
    </div>
  )
}

export function App() {

  const counter = useSelector((state) => state)
  const dispatch = useDispatch()

  return (
    <div>
      <div className="bg-slate-100">
        <div className="max-w-5xl mx-auto py-4">
          <h1 className="text-1xl">Microfrontend Architecture Example</h1>
        </div>
      </div>

      <div>
        <div className="max-w-5xl mx-auto py-4 grid grid-cols-12">
          <div className="col-span-2">
            <nav>
              <NavLink to="/" className="block">Home</NavLink>
              <NavLink to="/app1" className="block">App1</NavLink>
              <NavLink to="/app2" className="block">App2</NavLink>
              <NavLink to="/app3" className="block">App3</NavLink>
              <div className="mt-4">
                <p className="text-sm text-slate-500">Global State Example</p>
                <div className="flex">
                  <div className="px-2 border rounded bg-slate-100 cursor-pointer" onClick={() => { dispatch({ type: "DECREMENT" })}}>-</div>
                  <div className="px-2">{counter}</div>
                  <div className="px-2 border rounded bg-slate-100 cursor-pointer" onClick={() => { dispatch({ type: "INCREMENT" })}}>+</div>
                </div>
              </div>
            </nav>
          </div>
          <div className="col-span-10">
            <Suspense fallback="Loading...">
              <Routes>
                <Route path="" element={<Home />} />
                <Route path="app1" element={<App1Lazy />} />
                <Route path="app2" element={<App2Lazy />} />
                <Route path="app3/*" element={<App3Lazy />} />
              </Routes>
            </Suspense>
          </div>
        </div>
      </div>

    </div>
  )
}
