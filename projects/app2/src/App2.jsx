import React from 'react'
import { useSelector, useDispatch } from 'react-redux'

export default function App2() {
  const counter = useSelector((state) => state)
  return <div>
    <h1 className="text-4xl mb-4">App2</h1>
    <p className="mb-2">This project demonstrates sharing Redux global state. Because of how we have carefully constructed this application, the developer experience in accessing the global state is no different from accessing any other state. Down below we will see the same counter from our global state being easily accessed by this completely separate microfrontend application. The application reacts exactly like it would to state changes as if the state were local to it.</p>
    <p className="text-4xl text-bold">{counter}</p>
  </div>
}
