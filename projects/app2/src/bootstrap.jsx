import React from 'react'
import { createRoot } from 'react-dom'
import App2 from './App2.jsx'

const container = document.getElementById('root')
const root = createRoot(container)
root.render(<App2 />)


