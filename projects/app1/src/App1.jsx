import React from 'react'

export default function App1() {
  return <div>
    <h1 className="text-4xl mb-4">App1</h1>
    <p className="mb-2">This project is an example application of the minimal configuration required for a new microfrontend application.</p>
    <p>This project exists completely independent of other projects. It defines its own build script, dependencies, testing apparatus, etc. It would theoretically be a completely independently deployable artifact with no potential impact on other projects.</p>
  </div>
}
