import React from 'react'
import { createRoot } from 'react-dom'
import App1 from './App1.jsx'

const container = document.getElementById('root')
const root = createRoot(container)
root.render(<App1 />)


