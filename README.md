# Microfrontend Architecture

* Each project is its own, completely isolated project with its own build and test tools
* Supports Redux state sharing from shell to microfrontends

## Starting the service

* `npm start` and visit http://localhost:8080

## What's in this repo?

* The `shell` project is the base project that wraps the entire UI, providing global information to all projects
* The `app1` project is a minamally-viable project, effectively a starter template demonstrating the minimum specification to define a new project
* The `app2` project is more advanced, demonstrating internal code-splitting, its own store, etc

## Core Concepts

## Creating a new microfrontend

1. Create a new project in `projects`
1. Define your project (the following are suggestions, it can be anything)
    1. `npm init`
    1. `npm install -D webpack webpack-cli webpack-dev-server react react-dom react-router-dom babel-loader @babel/core @babel/preset-env @babel-react jest`
    1. Define the `start` script in your `package.json` (ie: `webpack serve`)
    1. Configure a port in your webpack.config.js that doesn't conflict
1. Add the project to webpack.config.js under ModuleFederationPlugin
1. Add the project to the root `start` script (this is a workaround until npm supports parallel scripts)
